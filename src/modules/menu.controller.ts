import { Controller, Get, Render } from "@nestjs/common";

@Controller()
export class MenuController {
	@Get("home")
	@Render("menu")
	index() {

	}

}