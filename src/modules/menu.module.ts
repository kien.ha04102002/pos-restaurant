import { MenuController } from './menu.controller';
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
	imports: [TypeOrmModule.forFeature([])],
	providers: [],
	controllers: [MenuController],
})
export class MenuModule { }